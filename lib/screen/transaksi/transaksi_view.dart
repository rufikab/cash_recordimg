import 'package:cash_recording/DBHelper.dart';
import 'package:cash_recording/constant.dart';
import 'package:cash_recording/models/index.dart';
import 'package:cash_recording/models/transaksiKategori.dart';
import 'package:cash_recording/screen/transaksi/transaksi_appbar.dart';
import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:sweetalert/sweetalert.dart';

class TransaksiPage extends StatefulWidget {
  final bool isUpdating;
  final TransaksiKategori _transaksi;
  TransaksiPage(this.isUpdating, this._transaksi, {Key key}) : super(key: key);
  @override
  _TransaksiPageState createState() => new _TransaksiPageState();
}

class _TransaksiPageState extends State<TransaksiPage> {
  bool isUpdating;
  TransaksiKategori _transaksi;
  Future<TransaksiList> transaksiList;
  List<Kategori> kategoriList;
  Kategori _kategori;
  TextEditingController controller = TextEditingController();
  String keterangan;
  String tanggal;
  DateTime _tanggal;
  String jumlah = '';
  String jenisTrans = "Pemasukan";
  int jenisKategori = 0;
  String title = "Sweet Alert";
  Color bgColor = Colors.blue;
  Color fgColor = Colors.white;
  IconData icLogo = Icons.star_border;

  final DateFormat format = DateFormat("dd-MM-yyyy");
  final oCcy = new NumberFormat("###,###", "id_ID");
  final formKey = new GlobalKey<FormState>();
  var dbHelper;

  @override
  void initState() {
    _transaksi = widget._transaksi;
    jenisKategori = 0;
    if (widget.isUpdating) {
      isUpdating = true;
      updateForm();
    } else {
      isUpdating = false;
    }
    dbHelper = DBHelper();
    refreshKategori();
    super.initState();
  }

  Future refreshKategori() async {
    _kategori = null;
    kategoriList = [];
    kategoriList = await (dbHelper.getKategoriList2(jenisKategori));
    setState(() {
      jenisKategori = 0;
    });
  }

  updateForm() {
    if (_transaksi.jenisTransaksi == "IN") {
      jenisTrans = "Pemasukan";
      jumlah = '${oCcy.format(_transaksi.pemasukan)}';
      jenisKategori = 0;
    } else {
      jenisTrans = "Pengeluaran";
      jumlah = '${oCcy.format(_transaksi.pengeluaran)}';
      jenisKategori = 1;
      kategoriList = dbHelper.getKategoriList2(jenisKategori);
    }
    keterangan = _transaksi.keterangan;

    DateFormat format = DateFormat("yyyy-MM-dd");
    tanggal = _transaksi.tanggal;
    _tanggal = format.parse(_transaksi.tanggal);
  }

  validate() {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      Transaksi t = new Transaksi();
      if (jenisTrans == "Pemasukan") {
        t.jenisTransaksi = "IN";
        t.pemasukan = double.parse(jumlah.replaceAll(".", ""));
        t.pengeluaran = 0;
      } else {
        t.jenisTransaksi = "OUT";
        t.pengeluaran = double.parse(jumlah.replaceAll(".", ""));
        t.pemasukan = 0;
      }
      t.keterangan = keterangan;
      t.tanggal = tanggal;
      t.kategori = jenisKategori;
      if (isUpdating) {
        t.id = _transaksi.id;
        dbHelper.update(t);
      } else {
        dbHelper.save(t);
      }
      return true;
    } else {
      return false;
    }
  }

  convertDateFromString(String strDate) {
    DateTime todayDate = DateTime.parse(strDate);
    print(todayDate);
    print(formatDate(todayDate, [yyyy, '/', mm, '/', dd]));
    return todayDate.toString();
  }

  form() {
    return new Form(
      key: formKey,
      child: Padding(
          padding: EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            verticalDirection: VerticalDirection.down,
            children: <Widget>[
              DropdownButton<String>(
                value: jenisTrans,
                icon: Icon(Icons.arrow_drop_down),
                iconSize: 10,
                elevation: 16,
                onChanged: (String newValue) {
                  setState(() {
                    jenisTrans = newValue;
                    if (jenisTrans == "Pemasukan") {
                      jenisKategori = 0;
                    } else {
                      jenisKategori = 1;
                    }
                    refreshKategori();
                  });
                },
                items: <String>['Pemasukan', 'Pengeluaran']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem(
                      child: Text(value,
                          style:
                              TextStyle(fontFamily: "Poppins", fontSize: 16.0)),
                      value: value);
                }).toList(),
              ),
              new DropdownButtonFormField<Kategori>(
                hint: Text('Kategori'),
                value: _kategori,
                onChanged: (Kategori value) {
                  setState(() {
                    _kategori = value;
                    jenisKategori = value.id;
                  });
                },
                items: kategoriList?.map((Kategori value) {
                      return new DropdownMenuItem<Kategori>(
                        value: value,
                        child: new Text(
                          value.keterangan,
                          style: new TextStyle(fontSize: 16.0),
                        ),
                      );
                    })?.toList() ??
                    [],
              ),
              DateTimeField(
                decoration: InputDecoration(labelText: 'Tanggal'),
                format: format,
                initialValue: _tanggal ?? null,
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                    context: context,
                    initialDate: currentValue ?? DateTime.now(),
                    firstDate: DateTime(1900),
                    lastDate: DateTime(2100),
                  );
                },
                onSaved: (val) =>
                    tanggal = formatDate(val, [yyyy, '-', mm, '-', dd]),
              ),
              TextFormField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(labelText: 'Keterangan'),
                initialValue: keterangan,
                validator: (val) =>
                    val.length == 0 ? 'Masukkan Keterangan' : null,
                onSaved: (val) => keterangan = val,
              ),
              TextFormField(
                inputFormatters: [
                  CurrencyTextInputFormatter(decimalDigits: 0, locale: "id")
                ],
                keyboardType: TextInputType.number,
                initialValue: jumlah,
                decoration: InputDecoration(labelText: 'Jumlah'),
                onSaved: (newValue) => jumlah = newValue,
              ),
              RaisedButton(
                color: ColorPalette.colorBtnActive,
                onPressed: () {
                  SweetAlert.show(context,
                      subtitle: "Kamu akan menyimpan data ini?",
                      style: SweetAlertStyle.confirm,
                      cancelButtonText: 'Batal',
                      confirmButtonText: 'Ya',
                      showCancelButton: true, onPress: (bool isConfirm) {
                    if (isConfirm) {
                      SweetAlert.show(context,
                          subtitle: "Menyimpan...",
                          style: SweetAlertStyle.loading);
                      bool returnd = validate();
                      if (returnd) {
                        new Future.delayed(new Duration(seconds: 2), () {
                          SweetAlert.show(context,
                              subtitle: "Sukses!",
                              style: SweetAlertStyle.success);
                        });
                        formKey.currentState?.reset();
                      } else {
                        SweetAlert.show(context,
                            subtitle: "Gagal Menyimpan!",
                            style: SweetAlertStyle.error);
                      }
                    } else {
                      SweetAlert.show(context,
                          subtitle: "Batal Menyimpan!",
                          style: SweetAlertStyle.error);
                    }
                    return false;
                  });
                },
                child: Text(isUpdating ? 'Update' : 'Simpan'),
              ),
            ],
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return new Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        child: Stack(
          children: [
            Positioned(
              top: 0.0,
              left: 0.0,
              right: 0.0,
              child: TransaksiAppBar(),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(10, 80, 10, 0),
              child: SingleChildScrollView(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[form()]),
              ),
            )
          ],
        ),
      ),
    );
  }
}
