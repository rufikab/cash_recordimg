import 'package:cash_recording/constant.dart';
import 'package:flutter/material.dart';

class TransaksiAppBar extends StatefulWidget {
  @override
  _BuildTransaksiAppBar createState() => _BuildTransaksiAppBar();
}

class _BuildTransaksiAppBar extends State<TransaksiAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: ColorPalette.colorPrimary,
      flexibleSpace: Stack(
        children: [
          Positioned(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 30, 20, 0),
              child: Column(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(left: 20.0),
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        "Transaksi Baru",
                        style: TextStyle(
                            fontSize: 18,
                            fontFamily: "Poppins-Medium",
                            color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
