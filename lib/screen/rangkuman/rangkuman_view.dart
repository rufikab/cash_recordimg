import 'package:cash_recording/constant.dart';
import 'package:cash_recording/screen/kategori/kategori_view.dart';
import 'package:cash_recording/screen/rangkuman/rangkuman_harian_view.dart';
import 'package:flutter/material.dart';
import 'package:cash_recording/landing/landingpage_view.dart';
import 'package:cash_recording/screen/custom_popup_menu.dart';
import 'package:cash_recording/models/index.dart';
import 'package:cash_recording/DBHelper.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart';

void main() => runApp(RangkumanPage(
      initialDate: DateTime.now(),
    ));

class RangkumanGlobal {
  static List<int> selectedKategori;
  static DateTime selectedDate;
}

List<CustomPopupMenu> choices = [
  CustomPopupMenu(
      title: 'Kategori', icon: Icons.category, iconColor: ColorPalette.pink),
  CustomPopupMenu(title: 'Popup Menu 2', icon: Icons.bookmark),
  CustomPopupMenu(title: 'Popup Menu 3', icon: Icons.settings),
];

class RangkumanPage extends StatefulWidget {
  final DateTime initialDate;
  RangkumanPage({Key key, @required this.initialDate}) : super(key: key);
  @override
  _RangkumanPageState createState() => new _RangkumanPageState();
}

class _RangkumanPageState extends State<RangkumanPage>
    with TickerProviderStateMixin {
  CustomPopupMenu _selectedChoices = choices[0];
  List<Kategori> kategoriListIN;
  List<Kategori> kategoriListOUT;
  DBHelper dbHelper;
  TabController _controller;
  DateTime selectedDate;
  String monthText;

  final GlobalKey<RangkumanHarianState> _key = GlobalKey();

  @override
  void initState() {
    selectedDate = DateTime.now();

    dbHelper = DBHelper();
    _controller = new TabController(length: 2, vsync: this);
    if (RangkumanGlobal.selectedKategori == null) {
      RangkumanGlobal.selectedKategori = [0];
    }
    RangkumanGlobal.selectedDate = DateTime.now();
    refreshKategori();
    super.initState();
  }

  Future refreshKategori() async {
    monthText = DateFormat('MMMM', 'id').format(selectedDate);
    kategoriListIN = [];
    kategoriListOUT = [];
    kategoriListIN = (await dbHelper.getKategoriList2(0));
    kategoriListOUT = (await dbHelper.getKategoriList2(1));
    setState(() {});
  }

  void _select(CustomPopupMenu choice) {
    if (choice.title == "Kategori") {
      Navigator.push(
          context,
          new MaterialPageRoute(
              builder: (BuildContext context) => new KategoriPage()));
    }
    setState(() {
      _selectedChoices = choice;
    });
  }

  void _onCategorySelected(bool selected, kategori_id) {
    if (selected == true) {
      setState(() {
        RangkumanGlobal.selectedKategori.add(kategori_id);
      });
    } else {
      setState(() {
        RangkumanGlobal.selectedKategori.remove(kategori_id);
      });
    }
  }

  _buildListKatIn() {
    return ListView.builder(
      itemCount: kategoriListIN.length,
      itemBuilder: (BuildContext context, index) {
        Kategori kategori = kategoriListIN[index];
        print(kategori);
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
            return CheckboxListTile(
              value: RangkumanGlobal.selectedKategori.contains(kategori.id),
              onChanged: (bool selected) {
                if (selected == true) {
                  setState(() {
                    RangkumanGlobal.selectedKategori.add(kategori.id);
                  });
                } else {
                  setState(() {
                    RangkumanGlobal.selectedKategori.remove(kategori.id);
                  });
                }
              },
              title: Text(kategori.keterangan),
            );
          },
        );
      },
    );
  }

  _buildListKatOut() {
    return ListView.builder(
      itemCount: kategoriListOUT.length,
      itemBuilder: (BuildContext context, index) {
        Kategori kategori = kategoriListOUT[index];
        print(kategori);
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
            return CheckboxListTile(
              value: RangkumanGlobal.selectedKategori.contains(kategori.id),
              onChanged: (bool selected) {
                if (selected == true) {
                  setState(() {
                    RangkumanGlobal.selectedKategori.add(kategori.id);
                  });
                } else {
                  setState(() {
                    RangkumanGlobal.selectedKategori.remove(kategori.id);
                  });
                }
              },
              title: Text(kategori.keterangan),
            );
          },
        );
      },
    );
  }

  dialogContent(BuildContext context) {
    return Container(
      color: ColorPalette.bgColorPrimary,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        //crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          ConstrainedBox(
            constraints: const BoxConstraints(
              minWidth: double.infinity,
            ),
            child: Container(
              color: ColorPalette.colorBox1,
              margin: EdgeInsets.only(bottom: 10),
              child: Padding(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                child: Text(
                  'FILTER DATA',
                  style: TextStyle(
                    fontFamily: "Burbank",
                    fontSize: 20,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(
              minWidth: double.infinity,
            ),
            child: Padding(
              padding: EdgeInsets.all(5),
              child: Container(
                child: Text(
                  'Silahkan pilih kategori yang ingin ditampilkan :',
                  style: TextStyle(fontSize: 14),
                  textAlign: TextAlign.start,
                ),
              ),
            ),
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(minWidth: double.infinity),
            child: Padding(
              padding: EdgeInsets.all(5),
              child: Container(
                child: TabBar(
                  indicatorColor: ColorPalette.colorPrimary,
                  controller: _controller,
                  tabs: [
                    Text(
                      "PEMASUKAN",
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: "Poppins-Medium",
                          color: Colors.grey),
                    ),
                    Text(
                      "PENGELUARAN",
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: "Poppins-Medium",
                          color: Colors.grey),
                    ),
                  ],
                ),
              ),
            ),
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(minWidth: double.infinity),
            child: Padding(
              padding: EdgeInsets.all(5),
              child: Container(
                height: 200,
                child: TabBarView(
                  children: [
                    _buildListKatIn(),
                    _buildListKatOut(),
                  ],
                  controller: _controller,
                ),
              ),
            ),
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(
              minWidth: double.infinity,
            ),
            child: Padding(
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Spacer(),
                    RaisedButton(
                      color: ColorPalette.grey,
                      onPressed: () {
                        Navigator.of(context, rootNavigator: true).pop();
                      },
                      child: Text("Batal"),
                    ),
                    RaisedButton(
                      color: ColorPalette.colorBtnActive,
                      onPressed: () {
                        Navigator.of(context, rootNavigator: true).pop();
                        _key.currentState.methodRefresh();
                      },
                      child: Text("Simpan"),
                    )
                  ],
                )),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        Locale('en'),
        Locale('zh'),
        Locale('fr'),
        Locale('es'),
        Locale('de'),
        Locale('ru'),
        Locale('ja'),
        Locale('ar'),
        Locale('fa'),
        Locale("es"),
        Locale("id")
      ],
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: ColorPalette.colorPrimary,
            bottom: TabBar(
              indicatorColor: ColorPalette.colorPrimary,
              tabs: [
                Text(
                  "Harian",
                  style: TextStyle(
                    fontSize: 18,
                    fontFamily: "Poppins-Medium",
                    color: Colors.white,
                  ),
                ),
                Text(
                  "Bulanan",
                  style: TextStyle(
                    fontSize: 18,
                    fontFamily: "Poppins-Medium",
                    color: Colors.white,
                  ),
                ),
                Text(
                  "Tahunan",
                  style: TextStyle(
                    fontSize: 18,
                    fontFamily: "Poppins-Medium",
                    color: Colors.white,
                  ),
                ),
              ],
            ),
            title: Text(
              'Rangkuman',
              style: TextStyle(
                fontSize: 18,
                fontFamily: "Poppins-Medium",
                color: Colors.white,
              ),
            ),
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    new MaterialPageRoute(
                        builder: (BuildContext context) => new LandingPage()));
              },
            ),
            actions: <Widget>[
              RaisedButton(
                color: ColorPalette.colorPrimary,
                onPressed: () {
                  showMonthPicker(
                    context: context,
                    firstDate: DateTime(DateTime.now().year - 1, 5),
                    lastDate: DateTime(DateTime.now().year + 1, 9),
                    initialDate: selectedDate ?? widget.initialDate,
                    locale: Locale("id"),
                  ).then((date) {
                    if (date != null) {
                      setState(() {
                        selectedDate = date;
                        RangkumanGlobal.selectedDate = selectedDate;
                        monthText =
                            DateFormat('MMMM', 'id').format(selectedDate);
                        _key.currentState.methodRefresh();
                      });
                    }
                  });
                },
                child: Text(
                  '${monthText.toString()}\n ${selectedDate?.year}',
                  style: TextStyle(
                    color: ColorPalette.bgColorPrimary,
                    fontSize: 14,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              IconButton(
                  icon: Icon(Icons.filter_list),
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) => _popUpFilter(),
                    );
                  }),
              PopupMenuButton(
                elevation: 8,
                initialValue: choices[0],
                onCanceled: () {
                  print('You have not chossed anything');
                },
                tooltip: 'This is tooltip',
                onSelected: _select,
                itemBuilder: (BuildContext context) {
                  return choices.map((CustomPopupMenu choice) {
                    return PopupMenuItem(
                      value: choice,
                      child: Container(
                        child: Row(
                          children: [
                            Icon(choice.icon,
                                size: 20, color: choice.iconColor),
                            //   Spacer(),
                            Padding(
                              padding: EdgeInsets.only(left: 10),
                              child: Text(
                                choice.title,
                                textAlign: TextAlign.left,
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  }).toList();
                },
              ),
            ],
          ),
          body: TabBarView(
            children: [
              RangkumanHarian(key: _key),
              Icon(Icons.directions_transit),
              Icon(Icons.directions_bike),
            ],
          ),
        ),
      ),
    );
  }

  Widget _popUpFilter() {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      insetPadding: EdgeInsets.all(15),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }
}
