import 'package:cash_recording/constant.dart';
import 'package:cash_recording/models/index.dart';
import 'package:cash_recording/models/transaksiByDate.dart';
import 'package:cash_recording/models/transaksiListByDate.dart';
import 'package:cash_recording/screen/beranda/beranda_appbar.dart';
import 'package:cash_recording/screen/transaksi/transaksi_view.dart';
import 'package:flutter/material.dart';
import 'package:cash_recording/DBHelper.dart';
import 'package:intl/intl.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:date_format/date_format.dart';
import 'package:cash_recording/screen/rangkuman/rangkuman_view.dart' as rv;

bool loading = true;

class Todo {
  final bool isUpdating;
  final int id;

  Todo(this.isUpdating, this.id);
}

class RangkumanHarian extends StatefulWidget {
  RangkumanHarian({Key key}) : super(key: key);
  @override
  RangkumanHarianState createState() => RangkumanHarianState();
}

class RangkumanHarianState extends State<RangkumanHarian> {
  BerandaAppBar _appBar = new BerandaAppBar();
  Future<TransaksiList> transaksiList;
  Future<TransaksiListByDate> transaksiListByDate;
  TextEditingController controller = TextEditingController();
  String name;
  int curId;
  DateTime initialDate;
  ScrollController _scrollController;
  int numberOfContainers = 10;
  List<bool> isSingleTapped = [];
  int i;
  final formKey = new GlobalKey<FormState>();
  final formatCurrency = new NumberFormat.simpleCurrency();
  var getTotalSaldo;
  final oCcy = NumberFormat.currency(locale: 'id', symbol: 'Rp. ');
  DBHelper dbHelper;
  Color color = Colors.blue;
  double x = 0.0;
  double y = 0.0;

  @override
  void initState() {
    loading = true;
    initialDate = DateTime.now();
    numberOfContainers = 10;
    _scrollController = ScrollController();
    dbHelper = DBHelper();
    refreshData();
    super.initState();
  }

  Future refreshData() async {
    loading = true;
    isSingleTapped.length = numberOfContainers;
    for (i = 0; i < numberOfContainers; i++) {
      isSingleTapped[i] = false;
    }
    transaksiListByDate = dbHelper.getTransaksiListByKat(
        rv.RangkumanGlobal.selectedKategori,
        rv.RangkumanGlobal.selectedDate.year,
        rv.RangkumanGlobal.selectedDate.month);
    setState(() {
      loading = false;
    });
  }

  showListTest(TransaksiListByDate listData, double height) {
    //isSingleTapped.length = listData.transaksiList.length;
    return Column(
      children: [
        Flexible(
          child: ListView.builder(
            controller: _scrollController,
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              print(isSingleTapped);
              TransaksiByDate transaksi = listData.transaksiList[index];
              print(transaksi);
              return _cardTransaksi(transaksi, index);
            },
            itemCount: listData.transaksiList.length,
          ),
        ),
      ],
    );
  }

  listDataTest(double height) {
    if (loading)
      return SpinKitDualRing(
        color: Colors.black,
        size: 50.0,
      );
    else
      return Container(
        height: (height * 0.8) - 287,
        child: FutureBuilder(
          future: transaksiListByDate,
          builder: (context, snapshot) {
            if (false == snapshot.hasData || null == snapshot.data) {
              return Text("Data not found");
            }
            print(snapshot.data);

            return showListTest(snapshot.data, height);
          },
        ),
      );
  }

  void changeColor(PointerEvent details) {
    setState(() {
      color = Colors.red;
      x = details.position.dx;
      y = details.position.dy;
    });
  }

  getDateDesc(String strDate) {
    DateTime date1 = DateTime.parse(strDate);
    String month = formatDate(date1, [MM]);
    String year = formatDate(date1, [yyyy]);
    String day = formatDate(date1, [dd]);
    List<String> data = ["", "", ""];
    data[0] = day;
    data[1] = month;
    data[2] = year;
    return data;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    print(size.height);
    return Container(
      margin: EdgeInsets.all(20),
      width: size.width,
      color: Colors.transparent,
      alignment: Alignment.center,
      child: Stack(
        children: [
          Column(
            children: <Widget>[
              listDataTest(size.height),
            ],
          ),
        ],
      ),
    );
  }

  methodRefresh() => refreshData();
  Widget _cardTransaksi(TransaksiByDate _transaksi, int i) {
    return GestureDetector(
      onTap: () {
        if (isSingleTapped[i] == true) {
          setState(() {
            isSingleTapped[i] = false;
          });
        } else {
          setState(() {
            isSingleTapped[i] = true;
          });
        }
      },
      child: Card(
        elevation: 4,
        color: ColorPalette.bgColorPrimary,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 3.0, horizontal: 16),
          child: Column(
            children: <Widget>[
              Container(
                //decoration: BoxDecoration(
                // border: Border(bottom: BorderSide(color: Colors.black))),
                child: Row(
                  children: <Widget>[
                    Text(
                      (getDateDesc(_transaksi.tanggal))[0],
                      style: TextStyle(
                        fontFamily: "Burbank",
                        color: ColorPalette.colorPrimary,
                        fontSize: 16.0,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.all(2),
                      child: Column(
                        children: <Widget>[
                          Text(
                            (getDateDesc(_transaksi.tanggal))[1],
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12.0,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            (getDateDesc(_transaksi.tanggal))[2],
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12.0,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 10),
                      child: Text(
                        '${oCcy.format(_transaksi.totalPemasukan)}',
                        style: TextStyle(
                          color: ColorPalette.green,
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Spacer(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          '${oCcy.format(_transaksi.totalPengeluaran)}',
                          style: TextStyle(
                            color: ColorPalette.red,
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Visibility(
                visible: isSingleTapped[i],
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(color: Colors.black38),
                    ),
                  ),
                  margin: EdgeInsets.only(top: 10),
                  child: Row(
                    children: [
                      Expanded(
                        child: ListView.builder(
                          controller: _scrollController,
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            TransaksiKategori transaksi =
                                _transaksi.transaksi[index];
                            return _cardDetailList(transaksi, index);
                          },
                          itemCount: _transaksi.transaksi.length,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _cardDetailList(TransaksiKategori transaksi, int i) {
    return GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext context) => _popUpAction(transaksi),
        );
      },
      child: Padding(
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Container(
          margin: EdgeInsets.only(top: 5),
          child: Row(
            children: <Widget>[
              if (transaksi.jenisTransaksi == "IN")
                Icon(Icons.arrow_right, size: 30, color: ColorPalette.green)
              else
                Icon(Icons.arrow_left, size: 30, color: ColorPalette.red),
              Text(
                transaksi.keterangan,
                style: TextStyle(color: Colors.black, fontSize: 14.0),
                textAlign: TextAlign.left,
              ),
              Spacer(),
              if (transaksi.jenisTransaksi == "IN")
                Text(
                  '${oCcy.format(transaksi.pemasukan)}',
                  style: TextStyle(color: Colors.black, fontSize: 14.0),
                )
              else
                Text(
                  '${oCcy.format(transaksi.pengeluaran)}',
                  style: TextStyle(color: Colors.black, fontSize: 14.0),
                )
            ],
          ),
        ),
      ),
    );
  }

  Widget _popUpAction(TransaksiKategori transaksi) {
    return new AlertDialog(
      contentPadding: EdgeInsets.all(10),
      //title: const Text('About Pop up'),
      content: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            ConstrainedBox(
              constraints: const BoxConstraints(minWidth: double.infinity),
              child: MouseRegion(
                onHover: changeColor,
                child: FlatButton(
                  color: ColorPalette.bgColorPrimary,
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                    Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => TransaksiPage(true, transaksi)),
                    );
                  },
                  textColor: Theme.of(context).primaryColor,
                  child: const Text(
                    'Edit',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
            ),
            ConstrainedBox(
              constraints: const BoxConstraints(minWidth: double.infinity),
              child: FlatButton(
                color: ColorPalette.bgColorPrimary,
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pop();
                  SweetAlert.show(context,
                      subtitle: "Kamu akan menghapus data ini?",
                      style: SweetAlertStyle.confirm,
                      cancelButtonText: 'Batal',
                      confirmButtonText: 'Ya',
                      showCancelButton: true, onPress: (bool isConfirm) {
                    if (isConfirm) {
                      if (isConfirm) {
                        SweetAlert.show(context,
                            subtitle: "Menghapus data...",
                            style: SweetAlertStyle.loading);
                        dbHelper.delete(transaksi.id);
                        refreshData();
                        new Future.delayed(new Duration(seconds: 2), () {
                          SweetAlert.show(context,
                              subtitle: "Data telah dihapus!",
                              style: SweetAlertStyle.success);
                        });
                      } else {
                        SweetAlert.show(context,
                            subtitle: "Batal!", style: SweetAlertStyle.error);
                      }
                      return false;
                    }
                  });
                },
                textColor: Theme.of(context).primaryColor,
                child: const Text(
                  'Hapus',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                  ),
                ),
              ),
            ),

            // _buildAboutText(),
            //_buildLogoAttribution(),
          ],
        ),
      ),
    );
  }
}
