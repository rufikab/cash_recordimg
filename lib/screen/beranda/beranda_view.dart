import 'package:cash_recording/screen/beranda/beranda_appbar.dart';
import 'package:cash_recording/screen/beranda/beranda_content_view.dart';
import 'package:flutter/material.dart';
import 'package:cash_recording/constant.dart';

class BerandaPage extends StatefulWidget {
  @override
  _BerandaPageState createState() => new _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: size.height,
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 0.0,
            left: 0.0,
            right: 0.0,
            child: BerandaAppBar(),
          ),
          BerandaContent()
        ],
      ),
    );
  }
}
