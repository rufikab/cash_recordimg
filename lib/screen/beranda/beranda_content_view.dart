import 'package:cash_recording/constant.dart';
import 'package:cash_recording/landing/landingpage_view.dart';
import 'package:cash_recording/models/index.dart';
import 'package:cash_recording/models/transaksiByDate.dart';
import 'package:cash_recording/models/transaksiKategori.dart';
import 'package:cash_recording/models/transaksiListByDate.dart';
import 'package:cash_recording/screen/beranda/beranda_appbar.dart';
import 'package:cash_recording/screen/transaksi/transaksi_view.dart';
import 'package:flutter/material.dart';
import 'package:cash_recording/models/transaksi.dart';
import 'package:cash_recording/DBHelper.dart';
import 'package:intl/intl.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:sweetalert/sweetalert.dart';

bool loading = true;

class Todo {
  final bool isUpdating;
  final int id;

  Todo(this.isUpdating, this.id);
}

class BerandaContent extends StatefulWidget {
  @override
  _BerandaContent createState() => _BerandaContent();
}

class _BerandaContent extends State<BerandaContent> {
  BerandaAppBar _appBar = new BerandaAppBar();
  Future<List<TransaksiKategori>> transaksiList;
  Future<TransaksiListByDate> transaksiListByDate;
  TextEditingController controller = TextEditingController();
  String name;
  int curId;
  ScrollController _scrollController;

  final formKey = new GlobalKey<FormState>();
  final formatCurrency = new NumberFormat.simpleCurrency();
  var getTotalSaldo;
  final oCcy = NumberFormat.currency(locale: 'id', symbol: 'Rp. ');
  DBHelper dbHelper;

  Color color = Colors.blue;
  double x = 0.0;
  double y = 0.0;

  @override
  void initState() {
    loading = true;
    _scrollController = ScrollController();

    dbHelper = DBHelper();
    refreshData();
    super.initState();
    // transaksiList = dbHelper.getTransaksiList();
    // loading = false;
  }

  Future refreshData() async {
    loading = true;
    transaksiList = dbHelper.getTransaksiListDetail();
    //transaksiListByDate = dbHelper.getTransaksiListTest();
    // _appBar.refresh();
    setState(() {
      loading = false;
    });
  }

  showList(List<TransaksiKategori> listData, double height) {
    return ListView.builder(
      controller: _scrollController,
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        TransaksiKategori transaksi = listData[index];
        print(transaksi);
        if (transaksi.jenisTransaksi == "OUT") {
          return _cardPengeluaran(transaksi);
        } else {
          return _cardPemasukan(transaksi);
        }
      },
      itemCount: listData.length,
    );
  }

  listData(double height) {
    if (loading)
      return SpinKitDualRing(
        color: Colors.black,
        size: 50.0,
      );
    else
      return Container(
        height: (height * 0.8) - 287,
        child: FutureBuilder(
          future: transaksiList,
          builder: (context, snapshot) {
            if (false == snapshot.hasData || null == snapshot.data) {
              return Text("Data not found");
            }
            print(snapshot.data);
            return showList(snapshot.data, height);
          },
        ),
      );
  }

  void changeColor(PointerEvent details) {
    setState(() {
      color = Colors.red;
      x = details.position.dx;
      y = details.position.dy;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    print(size.height);
    return Container(
      margin: EdgeInsets.fromLTRB(0, 325, 0, 0),
      width: size.width,
      color: Colors.transparent,
      alignment: Alignment.center,
      child: Stack(
        children: [
          Column(
            children: <Widget>[
              Opacity(
                opacity: 0.8,
                child: Container(
                  margin: EdgeInsets.fromLTRB(50, 5, 50, 5),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: ColorPalette.colorPrimary,
                    ),
                    color: ColorPalette.colorPrimary,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Text(
                      "Transaksi terakhir kamu",
                      style: TextStyle(
                          color: Colors.black, fontFamily: "Poppins_Bold"),
                    ),
                  ),
                ),
              ),
              listData(size.height),
              //listDataTest(size.height),
            ],
          ),
          FloatingActionButton(
              heroTag: "btnRefresh",
              child: Icon(Icons.refresh),
              onPressed: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => LandingPage()),
                );
              }),
        ],
      ),
    );
  }

  Widget _cardPemasukan(TransaksiKategori transaksi) {
    return GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext context) => _popUpAction(transaksi),
        );
      },
      child: Card(
        elevation: 4,
        color: ColorPalette.bgColorPrimary,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 3.0, horizontal: 16),
          child: Row(
            children: <Widget>[
              Icon(Icons.arrow_right, size: 30, color: ColorPalette.green),
              Container(
                margin: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      '${oCcy.format(transaksi.pemasukan)}',
                      style: TextStyle(color: Colors.black, fontSize: 14.0),
                      textAlign: TextAlign.left,
                    ),
                    Text(
                      transaksi.kategoriNama,
                      style: TextStyle(color: Colors.red, fontSize: 12.0),
                      textAlign: TextAlign.left,
                    ),
                    Text(
                      transaksi.keterangan,
                      style: TextStyle(color: Colors.black, fontSize: 12.0),
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
              ),
              Spacer(),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Pemasukan",
                      style: TextStyle(
                          color: ColorPalette.green,
                          fontSize: 16,
                          fontWeight: FontWeight.bold)),
                  SizedBox(height: 4),
                  Text(transaksi.tanggal,
                      style: TextStyle(fontSize: 12.0, color: Colors.black)),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _cardPengeluaran(TransaksiKategori transaksi) {
    return GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext context) => _popUpAction(transaksi),
        );
      },
      child: Card(
        elevation: 4,
        color: ColorPalette.bgColorPrimary,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 3.0, horizontal: 16),
          child: Row(
            children: <Widget>[
              Icon(Icons.arrow_left, size: 30, color: ColorPalette.red),
              Container(
                margin: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      '${oCcy.format(transaksi.pengeluaran)}',
                      style: TextStyle(color: Colors.black, fontSize: 14.0),
                      textAlign: TextAlign.left,
                    ),
                    Text(
                      transaksi.kategoriNama,
                      style: TextStyle(color: Colors.red, fontSize: 12.0),
                      textAlign: TextAlign.left,
                    ),
                    Text(
                      transaksi.keterangan,
                      style: TextStyle(color: Colors.black, fontSize: 12.0),
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
              ),
              Spacer(),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Pengeluaran",
                      style: TextStyle(
                          color: ColorPalette.red,
                          fontSize: 16,
                          fontWeight: FontWeight.bold)),
                  SizedBox(height: 4),
                  Text(transaksi.tanggal,
                      style: TextStyle(fontSize: 12.0, color: Colors.black)),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getKategori(int kategori_id) {
    return Text("halfo");
  }

  Widget _popUpAction(TransaksiKategori transaksi) {
    return new AlertDialog(
      contentPadding: EdgeInsets.all(10),
      //title: const Text('About Pop up'),
      content: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            ConstrainedBox(
              constraints: const BoxConstraints(minWidth: double.infinity),
              child: MouseRegion(
                onHover: changeColor,
                child: FlatButton(
                  color: ColorPalette.bgColorPrimary,
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => TransaksiPage(true, transaksi)),
                    );
                  },
                  textColor: Theme.of(context).primaryColor,
                  child: const Text(
                    'Edit',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
            ),
            ConstrainedBox(
              constraints: const BoxConstraints(minWidth: double.infinity),
              child: FlatButton(
                color: ColorPalette.bgColorPrimary,
                onPressed: () {
                  Navigator.of(context).pop();
                  SweetAlert.show(context,
                      subtitle: "Kamu akan menghapus data ini?",
                      style: SweetAlertStyle.confirm,
                      cancelButtonText: 'Batal',
                      confirmButtonText: 'Ya',
                      showCancelButton: true, onPress: (bool isConfirm) {
                    if (isConfirm) {
                      if (isConfirm) {
                        SweetAlert.show(context,
                            subtitle: "Menghapus data...",
                            style: SweetAlertStyle.loading);
                        dbHelper.delete(transaksi.id);
                        refreshData();
                        new Future.delayed(new Duration(seconds: 2), () {
                          SweetAlert.show(context,
                              subtitle: "Data telah dihapus!",
                              style: SweetAlertStyle.success);
                        });
                      } else {
                        SweetAlert.show(context,
                            subtitle: "Batal!", style: SweetAlertStyle.error);
                      }
                      return false;
                    }
                  });
                },
                textColor: Theme.of(context).primaryColor,
                child: const Text(
                  'Hapus',
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),

            // _buildAboutText(),
            //_buildLogoAttribution(),
          ],
        ),
      ),
    );
  }
}
