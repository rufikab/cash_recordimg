import 'package:cash_recording/constant.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:cash_recording/DBHelper.dart';

bool loadingHeader = false;
num totalSaldo = 0.0;
num totalPemasukan = 0.0;
num totalPengeluaran = 0.0;

class BerandaAppBar extends StatefulWidget {
  final _BuildBerandaAppBar build = _BuildBerandaAppBar();
  void refresh() {
    build._calcTotal();
  }

  @override
  _BuildBerandaAppBar createState() => build;
}

class _BuildBerandaAppBar extends State<BerandaAppBar> {
  DBHelper dbHelper;
  final oCcy = NumberFormat.currency(locale: 'id', symbol: 'Rp. ');
  //final oCcy = NumberFormat.currency(locale: 'id', symbol: 'Rp. ',decimalDigits: 0);
  @override
  void initState() {
    super.initState();
    dbHelper = DBHelper();
    _calcTotal();
  }

  void _calcTotal() async {
    loadingHeader = true;
    dbHelper = DBHelper();
    var getData = (await dbHelper.totalSaldo());
    var sisa = getData[0]['sisa'];
    var pemasukan = getData[0]['totalPemasukan'];
    var pengeluaran = getData[0]['totalPengeluaran'];
    totalSaldo = sisa;
    totalPemasukan = pemasukan;
    totalPengeluaran = pengeluaran;
    loadingHeader = false;

    setState(() {});
  }

  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: ColorPalette.colorPrimary,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(50),
        ),
      ),
      flexibleSpace: Stack(
        children: [
          Positioned(
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  _row1(),
                  _row2(),
                  _row3(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _row1() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            alignment: Alignment.center,
            margin: EdgeInsets.only(top: 10),
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Text(
                "Rangkuman Kas Kamu",
                style: TextStyle(
                  fontFamily: "Burbank",
                  fontSize: 20.0,
                  color: ColorPalette.bgColorPrimary,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _row2() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Opacity(
              opacity: 0.8,
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: ColorPalette.bgColorPrimary,
                  ),
                  color: ColorPalette.bgColorPrimary,
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Text(
                        "Sisa Saldo Kas Kamu",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: "Poppins_Bold",
                          fontSize: 14.0,
                        ),
                      ),
                      Text(
                        '${oCcy.format(totalSaldo)}',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: "Poppins",
                          fontSize: 18.0,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _row3() {
    if (loadingHeader)
      return SpinKitDualRing(
        color: Colors.white,
        size: 50.0,
      );
    else
      return Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Opacity(
                opacity: 0.8,
                child: Container(
                  margin: EdgeInsets.all(5),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: ColorPalette.bgColorPrimary,
                    ),
                    color: ColorPalette.bgColorPrimary,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 5, 0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Image.asset(
                                "assets/images/saving-money.jpg",
                                width: 50,
                                height: 50,
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Pemasukan",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontFamily: "Poppins_Bold",
                                  fontSize: 13.0,
                                ),
                              ),
                              Text(
                                '${oCcy.format(totalPemasukan)}',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontFamily: "Poppins",
                                  fontSize: 14.0,
                                  color: Colors.black,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Opacity(
                opacity: 0.8,
                child: Container(
                  margin: EdgeInsets.all(5),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: ColorPalette.bgColorPrimary,
                    ),
                    color: ColorPalette.bgColorPrimary,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 5, 0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Image.asset(
                                "assets/images/saving-money.jpg",
                                width: 50,
                                height: 50,
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Pengeluaran",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontFamily: "Poppins_Bold",
                                  fontSize: 13.0,
                                ),
                              ),
                              Text(
                                '${oCcy.format(totalPengeluaran)}',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontFamily: "Poppins",
                                  fontSize: 14.0,
                                  color: Colors.black,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
  }
}
