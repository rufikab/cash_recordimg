import 'package:flutter/material.dart';

class CustomPopupMenu {
  CustomPopupMenu({this.title, this.icon, this.iconColor});

  String title;
  IconData icon;
  Color iconColor;
}
