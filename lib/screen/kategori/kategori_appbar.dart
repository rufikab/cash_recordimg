import 'package:cash_recording/constant.dart';
import 'package:flutter/material.dart';

class KategoriAppBar extends StatefulWidget {
  @override
  _BuildKategoriAppBar createState() => _BuildKategoriAppBar();
}

class _BuildKategoriAppBar extends State<KategoriAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: ColorPalette.colorPrimary,
      flexibleSpace: Stack(
        children: [
          Positioned(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 30, 20, 0),
              child: Column(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(left: 20.0),
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        "Kategori",
                        style: TextStyle(
                            fontSize: 18,
                            fontFamily: "Poppins-Medium",
                            color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
