import 'package:cash_recording/constant.dart';
import 'package:cash_recording/screen/kategori/kategori_appbar.dart';
import 'package:flutter/material.dart';
import 'package:cash_recording/models/kategori.dart';
import 'package:cash_recording/models/kategoriList.dart';
import 'package:cash_recording/DBHelper.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class KategoriPage extends StatefulWidget {
  @override
  _KategoriPageState createState() => new _KategoriPageState();
}

class _KategoriPageState extends State<KategoriPage> {
  int jenis = 0;
  int _id = 0;
  bool isUpdating;
  Future<KategoriList> kategoriList;
  ScrollController _scrollController;
  DBHelper dbHelper;

  String nama;

  final formKey = new GlobalKey<FormState>();

  @override
  void initState() {
    dbHelper = DBHelper();
    isUpdating = false;
    _id = 0;
    _scrollController = ScrollController();
    refreshData();
    super.initState();
  }

  Future refreshData() async {
    kategoriList = dbHelper.getKategoriList(jenis);
    setState(() {});
  }

  listData() {
    return Container(
      transform: Matrix4.translationValues(0.0, -20.0, 0.0),
      //  height: (height * 0.8) - 287,
      child: FutureBuilder(
        future: kategoriList,
        builder: (context, snapshot) {
          if (false == snapshot.hasData || null == snapshot.data) {
            return Text("Data not found");
          }
          print(snapshot.data);
          return showList(snapshot.data);
        },
      ),
    );
  }

  showList(KategoriList listData) {
    return ListView.builder(
      controller: _scrollController,
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        Kategori kategori = listData.kategoriList[index];
        print(kategori);
        return _slideCardKategori(kategori);
      },
      itemCount: listData.kategoriList.length,
    );
  }

  _formKategori() {
    return new AlertDialog(
      contentPadding: EdgeInsets.all(10),
      content: Container(
          child: Form(
        key: formKey,
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            verticalDirection: VerticalDirection.down,
            children: <Widget>[
              TextFormField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(labelText: 'Nama'),
                initialValue: nama,
                validator: (val) => val.length == 0 ? 'Masukkan nama' : null,
                onSaved: (val) => nama = val,
              ),
              RaisedButton(
                color: ColorPalette.colorBtnActive,
                onPressed: () {
                  Navigator.of(context).pop();
                  bool saveK = saveData();

                  if (saveK) {
                    formKey.currentState?.reset();
                    refreshData();
                  } else {
                    SweetAlert.show(context,
                        subtitle: "Gagal Menyimpan!",
                        style: SweetAlertStyle.error);
                  }
                  return false;
                },
                child: Text('Simpan'),
              )
            ],
          ),
        ),
      )),
    );
  }

  saveData() {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      Kategori k = new Kategori();
      k.jenis = jenis;
      k.keterangan = nama;
      if (isUpdating) {
        k.id = _id;
        dbHelper.updateKategori(k);
      } else {
        dbHelper.saveKategori(k);
      }
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return new Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        child: Stack(
          children: [
            Positioned(
              top: 0.0,
              left: 0.0,
              right: 0.0,
              child: KategoriAppBar(),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 80, 0, 0),
              child: SingleChildScrollView(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Card(
                        elevation: 4,
                        color: ColorPalette.bgColorPrimary,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 3.0, horizontal: 16),
                          child: Row(
                            children: <Widget>[
                              DropdownButton(
                                value: jenis,
                                items: [
                                  DropdownMenuItem(
                                    child: Text("Pemasukan"),
                                    value: 0,
                                  ),
                                  DropdownMenuItem(
                                    child: Text("Pengeluaran"),
                                    value: 1,
                                  ),
                                ],
                                onChanged: (value) {
                                  setState(() {
                                    jenis = value;
                                    refreshData();
                                  });
                                },
                              ),
                              Spacer(),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  IconButton(
                                      icon: Icon(
                                        Icons.add_circle,
                                        color: ColorPalette.colorBtnActive,
                                        size: 30,
                                      ),
                                      onPressed: () {
                                        nama = '';
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) =>
                                              _formKategori(),
                                        );
                                      })
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      listData()
                    ]),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _slideCardKategori(Kategori kategori) {
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.25,
      child: Container(
        color: ColorPalette.bgColorPrimary,
        child: Card(
          elevation: 4,
          color: ColorPalette.bgColorPrimary,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 3.0, horizontal: 0),
            child: ListTile(
              title: Text(kategori.keterangan),
            ),
          ),
        ),
      ),
      secondaryActions: <Widget>[
        IconSlideAction(
          caption: 'Edit',
          color: ColorPalette.grey,
          icon: Icons.edit,
          onTap: () {
            isUpdating = true;
            nama = kategori.keterangan;
            _id = kategori.id;
            showDialog(
              context: context,
              builder: (BuildContext context) => _formKategori(),
            );
          },
        ),
        IconSlideAction(
          caption: 'Hapus',
          color: ColorPalette.red,
          icon: Icons.delete,
          onTap: () {
            SweetAlert.show(context,
                subtitle: "Kamu akan menghapus data ini?",
                style: SweetAlertStyle.confirm,
                cancelButtonText: 'Batal',
                confirmButtonText: 'Ya',
                showCancelButton: true, onPress: (bool isConfirm) {
              if (isConfirm) {
                if (isConfirm) {
                  SweetAlert.show(context,
                      subtitle: "Menghapus data...",
                      style: SweetAlertStyle.loading);
                  dbHelper.deleteKategori(kategori.id);
                  refreshData();
                  new Future.delayed(new Duration(seconds: 2), () {
                    SweetAlert.show(context,
                        subtitle: "Data telah dihapus!",
                        style: SweetAlertStyle.success);
                  });
                } else {
                  SweetAlert.show(context,
                      subtitle: "Batal!", style: SweetAlertStyle.error);
                }
                return false;
              }
            });
          },
        ),
      ],
    );
  }
}
