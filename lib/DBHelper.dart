import 'dart:async';
import 'dart:io' as io;
import 'package:cash_recording/models/index.dart';
import 'package:cash_recording/models/kategoriList.dart';
import 'package:cash_recording/models/transaksiKategori.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:cash_recording/models/transaksi.dart';
import 'package:cash_recording/models/transaksiList.dart';
import 'package:cash_recording/models/transaksiByDate.dart';
import 'package:cash_recording/models/transaksiListByDate.dart';
import 'package:collection/collection.dart';

class DBHelper {
  static Database _db;

  //for transaksi table
  static const String TABLE = 'transaksi';
  static const String ID = 'id';
  static const String JENIS_TRANSAKSI = 'jenisTransaksi';
  static const String KETERANGAN = 'keterangan';
  static const String TANGGAL = 'tanggal';
  static const String KATEGORI = 'kategori';
  static const String PEMASUKAN = 'pemasukan';
  static const String PENGELUARAN = 'pengeluaran';
  static const String DB_NAME = 'cash_recording.db';

  //for kategori's table
  static const String TABLE1 = 'kategori';
  static const String ID1 = 'id';
  static const String KETERANGAN1 = 'keterangan';
  static const String JENIS1 = 'jenis';

  //Initialize the Database
  Future<Database> get db async {
    if (null != _db) {
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, DB_NAME);
    // deleteDatabase(path);
    var db = await openDatabase(path,
        version: 4, onCreate: _onCreate, onUpgrade: _onUpgrade);
    return db;
  }

  _onCreate(Database db, int version) async {
    //Create DB Table
    await db.execute(
        "CREATE TABLE $TABLE ($ID INTEGER PRIMARY KEY, $JENIS_TRANSAKSI TEXT, $KETERANGAN TEXT, $TANGGAL TEXT,$KATEGORI INT, $PEMASUKAN NUMERIC(12,2),$PENGELUARAN NUMERIC(12,2))");
    await db.execute(
        "CREATE TABLE $TABLE1 ($ID1 INTEGER PRIMARY KEY, $KETERANGAN1 TEXT, $JENIS1 INTEGER)");
  }

  _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.execute("DROP TABLE IF EXISTS $TABLE");
      await db.execute(
          "CREATE TABLE $TABLE ($ID INTEGER PRIMARY KEY, $JENIS_TRANSAKSI TEXT, $KETERANGAN TEXT, $TANGGAL TEXT,$KATEGORI INT, $PEMASUKAN NUMERIC(12,2),$PENGELUARAN NUMERIC(12,2))");
    }
  }

  // Method to Insert Transaksi record to Database
  Future<Transaksi> save(Transaksi transaksi) async {
    var dbClient = await db;
    transaksi.id = await dbClient.insert(TABLE, transaksi.toJson());
    return transaksi;
  }

  //Method to return all transakso from the DB
  Future<TransaksiList> getTransaksiList() async {
    var dbClient = await db;
    //specify the column names you want in the result set
    List<Map> maps = await dbClient.rawQuery(
        'SELECT t.* ,kategori.keterangan as kategoriNama FROM $TABLE t left join kategori on kategori.id == t.kategori ORDER by TANGGAL DESC');
    print(maps);
    TransaksiList allTransaksi = TransaksiList();
    List<TransaksiKategori> transaksiList = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        transaksiList.add(TransaksiKategori.fromJson(maps[i]));
      }
    }
    allTransaksi.transaksiList = transaksiList;
    return allTransaksi;
  }

  Future<TransaksiListByDate> getTransaksiListByDate() async {
    var dbClient = await db;
    //specify the column names you want in the result set
    List<Map> maps = await dbClient.rawQuery(
        'SELECT t.* ,kategori.keterangan as kategoriNama FROM $TABLE t left join kategori on kategori.id == t.kategori ORDER by TANGGAL DESC');
    Map groupByDate = groupBy(maps, (obj) => obj['tanggal']);
    /* .map((k, v) => MapEntry(k, v.map((item) {
              item.remove('release_date');
              return item;
            })));*/

    Map groupedAndSum = Map();

    groupByDate.forEach((k, v) {
      groupedAndSum[k] = {
        'tanggal': k,
        'transaksi': v,
        'totalPemasukan':
            v.fold(0, (prev, element) => prev + element['pemasukan']),
        'totalPengeluaran':
            v.fold(0, (prev, element) => prev + element['pengeluaran']),
      };
    });
    TransaksiListByDate allTransaksi = TransaksiListByDate();
    List<TransaksiByDate> transaksiList = [];
    groupedAndSum.forEach((k, v) {
      print(k.toString());
      print(v);
      transaksiList.add(TransaksiByDate.fromJson(v));
    });
    allTransaksi.transaksiList = transaksiList;
    return allTransaksi;
  }

  Future<TransaksiListByDate> getTransaksiListByKat(
      List<int> kat, int year, int month) async {
    var dbClient = await db;
    List<Map> maps;
    if (kat[0] == 0 && kat.length < 2) {
      maps = await dbClient.rawQuery(
          'SELECT t.* ,CAST(strftime("%m",date(t.tanggal)) AS INTEGER) AS month,CAST(strftime("%Y",date(t.tanggal)) AS INTEGER) AS year ,kategori.keterangan as kategoriNama FROM $TABLE t left join kategori on kategori.id == t.kategori WHERE month == $month AND year == $year ORDER by TANGGAL DESC');
    } else {
      //specify the column names you want in the result set
      maps = await dbClient.rawQuery(
          'SELECT t.* ,CAST(strftime("%m",date(t.tanggal)) AS INTEGER) AS month,CAST(strftime("%Y",date(t.tanggal)) AS INTEGER) AS year ,kategori.keterangan as kategoriNama FROM $TABLE t left join kategori on kategori.id == t.kategori WHERE t.kategori IN (\'' +
              (kat.join('\',\'')) +
              '\') AND month == $month AND year == $year ORDER by TANGGAL DESC');
    }
    //  var newMap = groupBy(maps, (obj) => obj['tanggal']);
    // print(newMap);
    Map groupByDate = groupBy(maps, (obj) => obj['tanggal']);
    /* .map((k, v) => MapEntry(k, v.map((item) {
              item.remove('release_date');
              return item;
            })));*/

    Map groupedAndSum = Map();

    groupByDate.forEach((k, v) {
      groupedAndSum[k] = {
        'tanggal': k,
        'transaksi': v,
        'totalPemasukan':
            v.fold(0, (prev, element) => prev + element['pemasukan']),
        'totalPengeluaran':
            v.fold(0, (prev, element) => prev + element['pengeluaran']),
      };
    });
    //  print(groupedAndSum[0]['list'].toString());
    TransaksiListByDate allTransaksi = TransaksiListByDate();
    List<TransaksiByDate> transaksiList = [];
    groupedAndSum.forEach((k, v) {
      print(k.toString());
      print(v);
      transaksiList.add(TransaksiByDate.fromJson(v));
    });
    allTransaksi.transaksiList = transaksiList;
    return allTransaksi;
  }

  Future<List<TransaksiKategori>> getTransaksiListDetail() async {
    var dbClient = await db;
    List<Map> maps = await dbClient.rawQuery(
        'select transaksi.*,kategori.keterangan as kategoriNama from transaksi left join kategori on transaksi.kategori == kategori.id group by transaksi.id');
    print(maps);
    List<TransaksiKategori> transaksiList = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        transaksiList.add(TransaksiKategori.fromJson(maps[i]));
      }
      return transaksiList;
    } else {
      return null;
    }
  }

  Future<TransaksiListByDate> getTransaksiListGroupByWeek(
      List<int> kat, int year, int month) async {
    var dbClient = await db;
    //specify the column names you want in the result set
    List<Map> maps = await dbClient.rawQuery(
        'SELECT t.id,t.jenis_transaksi,t.tanggal as tanggal_lengkap, t.keterangan, t.pemasukan,t.pengeluaran,t.kategori,' +
            'CAST(strftime("%m",date(t.tanggal)) AS INTEGER) AS tanggal,CAST(strftime("%Y",date(t.tanggal)) AS INTEGER) AS year ,kategori.keterangan as kategoriNama FROM $TABLE t left join kategori on kategori.id == t.kategori WHERE t.kategori IN (\'' +
            (kat.join('\',\'')) +
            '\') AND tanggal == $month AND year == $year ORDER by TANGGAL DESC');

    //  var newMap = groupBy(maps, (obj) => obj['tanggal']);
    // print(newMap);
    Map groupByDate = groupBy(maps, (obj) => obj['tanggal']);
    /* .map((k, v) => MapEntry(k, v.map((item) {
              item.remove('release_date');
              return item;
            })));*/

    Map groupedAndSum = Map();

    groupByDate.forEach((k, v) {
      groupedAndSum[k] = {
        'tanggal': k,
        'transaksi': v,
        'totalPemasukan':
            v.fold(0, (prev, element) => prev + element['pemasukan']),
        'totalPengeluaran':
            v.fold(0, (prev, element) => prev + element['pengeluaran']),
      };
    });
    //  print(groupedAndSum[0]['list'].toString());
    TransaksiListByDate allTransaksi = TransaksiListByDate();
    List<TransaksiByDate> transaksiList = [];
    groupedAndSum.forEach((k, v) {
      print(k.toString());
      print(v);
      transaksiList.add(TransaksiByDate.fromJson(v));
    });
    allTransaksi.transaksiList = transaksiList;
    return allTransaksi;
  }

  //Method to delete an transaksi from database
  Future<int> delete(int id) async {
    var dbClient = await db;
    return await dbClient.delete(TABLE, where: '$ID=?', whereArgs: [id]);
  }

  //get Total Saldo
  Future<List> totalSaldo() async {
    var dbClient = await db;
    var totalPemasukan = 0;
    var totalPengeluaran;
    var sisa;
    var countPemasukan = await dbClient.rawQuery(
        'SELECT SUM(pemasukan) as totalPemasukan FROM $TABLE where jenisTransaksi="IN"');
    var countPengeluaran = await dbClient.rawQuery(
        'SELECT SUM(pengeluaran) as totalPengeluaran FROM $TABLE where jenisTransaksi="OUT"');
    totalPemasukan = countPemasukan.toList()[0]['totalPemasukan'] ?? 0;
    totalPengeluaran = countPengeluaran.toList()[0]['totalPengeluaran'] ?? 0;
    sisa = totalPemasukan - totalPengeluaran;
    var data = [
      {
        'totalPemasukan': totalPemasukan,
        'totalPengeluaran': totalPengeluaran,
        'sisa': sisa
      }
    ];
    return data;
  }

  //Method to update a Transaksi in the Database
  Future<int> update(Transaksi transaksi) async {
    var dbClient = await db;
    return await dbClient.update(TABLE, transaksi.toJson(),
        where: '$ID = ?', whereArgs: [transaksi.id]);
  }

  Future<KategoriList> getKategoriList(int jenis) async {
    var dbClient = await db;
    //specify the column names you want in the result set
    List<Map> maps = await dbClient.rawQuery(
        'SELECT * FROM $TABLE1 WHERE $JENIS1 == $jenis ORDER by ID DESC');

    KategoriList allKategori = KategoriList();
    List<Kategori> kategoriList = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        kategoriList.add(Kategori.fromJson(maps[i]));
      }
    }
    allKategori.kategoriList = kategoriList;
    return allKategori;
  }

  Future<List<Kategori>> getKategoriList2(int jenis) async {
    var dbClient = await db;
    //specify the column names you want in the result set
    List<Map> maps = await dbClient.rawQuery(
        'SELECT * FROM $TABLE1 WHERE $JENIS1 == $jenis ORDER by ID DESC');
    List<Kategori> kategoriList = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        kategoriList.add(Kategori.fromJson(maps[i]));
      }
    }
    return kategoriList;
  }

  Future<Kategori> saveKategori(Kategori kategori) async {
    var dbClient = await db;
    kategori.id = await dbClient.insert(TABLE1, kategori.toJson());
    return kategori;
  }

  Future<int> deleteKategori(int id) async {
    var dbClient = await db;
    return await dbClient.delete(TABLE1, where: '$ID=?', whereArgs: [id]);
  }

  Future<int> updateKategori(Kategori kategori) async {
    var dbClient = await db;
    return await dbClient.update(TABLE1, kategori.toJson(),
        where: '$ID = ?', whereArgs: [kategori.id]);
  }

  //Method to Truncate the table
  Future<void> truncateTable() async {
    var dbClient = await db;
    return await dbClient.delete(TABLE);
  }

  //Method to Close Database
  Future close() async {
    var dbClient = await db;
    dbClient.close();
  }
}
