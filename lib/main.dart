import 'package:cash_recording/landing/landingpage_view.dart';
import 'package:cash_recording/launcher/launcher_view.dart';
import 'package:cash_recording/login/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Pencatatan Kas',
      theme: ThemeData(
        fontFamily: 'Poppins',
        primaryColor: Colors.black,
        accentColor: Colors.black,
      ),
      initialRoute: '/LauncherPage',
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case '/LauncherPage':
            return PageTransition(
              child: LauncherPage(),
              type: PageTransitionType.fade,
              settings: settings,
            );
            break;
          case '/LandingPage':
            return PageTransition(
              child: LandingPage(),
              type: PageTransitionType.fade,
              settings: settings,
            );
            break;
          default:
        }
      },
      // home: new LoginPage(),
    );
  }
}
