import 'package:cash_recording/constant.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class LauncherPage extends StatefulWidget {
  @override
  _LauncherPageState createState() => new _LauncherPageState();
}

class _LauncherPageState extends State<LauncherPage>
    with TickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _animation;
  @override
  initState() {
    Future.delayed(Duration(seconds: 5), () {
      Navigator.pushNamed(context, '/LandingPage');
    });
    super.initState();
    _controller = AnimationController(
      duration: const Duration(milliseconds: 2000),
      vsync: this,
      value: 0.1,
    );
    _animation =
        CurvedAnimation(parent: _controller, curve: Curves.bounceInOut);
    _controller.forward();
  }

  @override
  dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: ColorPalette.bgColorPrimary, body: _launcherContent());
  }

  Widget _launcherContent() {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        margin: EdgeInsets.all(10.0),
        child: ScaleTransition(
          scale: _animation,
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image(image: AssetImage("assets/images/saving-money.jpg")),
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: EdgeInsets.all(15.0),
              ),
              Text(
                "Atur Keuanganmu !",
                style: TextStyle(
                    fontFamily: "PC",
                    fontSize: 30.0,
                    color: ColorPalette.colorPrimary),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
