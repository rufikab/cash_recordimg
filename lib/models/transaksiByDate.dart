import 'package:json_annotation/json_annotation.dart';
import 'transaksiKategori.dart';
part 'transaksiByDate.g.dart';

@JsonSerializable(nullable: false)
class TransaksiByDate {
  String tanggal;
  List<TransaksiKategori> transaksi;
  num totalPemasukan;
  num totalPengeluaran;

  TransaksiByDate(
      this.tanggal, this.transaksi, this.totalPemasukan, this.totalPengeluaran);

  factory TransaksiByDate.fromJson(Map<String, dynamic> json) =>
      _$TransaksiByDateFromJson(json);
  Map<String, dynamic> toJson() => _$TransaksiByDateToJson(this);
}
