import 'package:json_annotation/json_annotation.dart';
import 'kategori.dart';
part 'kategoriList.g.dart';

@JsonSerializable()
class KategoriList {
  KategoriList();

  List<Kategori> kategoriList;

  factory KategoriList.fromJson(Map<String, dynamic> json) =>
      _$KategoriListFromJson(json);
  Map<String, dynamic> toJson() => _$KategoriListToJson(this);
}
