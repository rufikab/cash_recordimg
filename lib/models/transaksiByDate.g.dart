// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaksiByDate.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransaksiByDate _$TransaksiByDateFromJson(Map<String, dynamic> json) {
  return TransaksiByDate(
      json['tanggal'] as String,
      (json['transaksi'] as List)
          .map((e) => TransaksiKategori.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['totalPemasukan'] as num,
      json['totalPengeluaran'] as num);
}

Map<String, dynamic> _$TransaksiByDateToJson(TransaksiByDate instance) =>
    <String, dynamic>{
      'tanggal': instance.tanggal,
      'transaksi': instance.transaksi,
      'totalPemasukan': instance.totalPemasukan,
      'totalPengeluaran': instance.totalPengeluaran
    };
