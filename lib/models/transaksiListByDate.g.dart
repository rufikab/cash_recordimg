// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaksiListByDate.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransaksiListByDate _$TransaksiListByDateFromJson(Map<String, dynamic> json) {
  return TransaksiListByDate()
    ..transaksiList = (json['transaksiList'] as List)
        .map((e) => TransaksiByDate.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$TransaksiListByDateToJson(
        TransaksiListByDate instance) =>
    <String, dynamic>{'transaksiList': instance.transaksiList};
