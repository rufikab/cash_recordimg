// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaksiList.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransaksiList _$TransaksiListFromJson(Map<String, dynamic> json) {
  return TransaksiList()
    ..transaksiList = (json['transaksiList'] as List)
        ?.map((e) => e == null
            ? null
            : TransaksiKategori.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$TransaksiListToJson(TransaksiList instance) =>
    <String, dynamic>{'transaksiList': instance.transaksiList};
