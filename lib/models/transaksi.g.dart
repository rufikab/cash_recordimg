// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaksi.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Transaksi _$TransaksiFromJson(Map<String, dynamic> json) {
  return Transaksi()
    ..id = json['id'] as int
    ..jenisTransaksi = json['jenisTransaksi'] as String
    ..keterangan = json['keterangan'] as String
    ..tanggal = json['tanggal'] as String
    ..kategori = json['kategori'] as int
    ..pemasukan = json['pemasukan'] as num
    ..pengeluaran = json['pengeluaran'] as num;
}

Map<String, dynamic> _$TransaksiToJson(Transaksi instance) => <String, dynamic>{
      'id': instance.id,
      'jenisTransaksi': instance.jenisTransaksi,
      'keterangan': instance.keterangan,
      'tanggal': instance.tanggal,
      'kategori': instance.kategori,
      'pemasukan': instance.pemasukan,
      'pengeluaran': instance.pengeluaran
    };
