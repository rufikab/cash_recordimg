import 'package:json_annotation/json_annotation.dart';

part 'kategori.g.dart';

@JsonSerializable()
class Kategori {
  Kategori();

  int id;
  String keterangan;
  int jenis;

  factory Kategori.fromJson(Map<String, dynamic> json) =>
      _$KategoriFromJson(json);
  Map<String, dynamic> toJson() => _$KategoriToJson(this);
}
