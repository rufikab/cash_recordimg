import 'package:json_annotation/json_annotation.dart';
import "transaksiKategori.dart";
part 'transaksiList.g.dart';

@JsonSerializable()
class TransaksiList {
  TransaksiList();

  List<TransaksiKategori> transaksiList;

  factory TransaksiList.fromJson(Map<String, dynamic> json) =>
      _$TransaksiListFromJson(json);
  Map<String, dynamic> toJson() => _$TransaksiListToJson(this);
}
