import 'package:json_annotation/json_annotation.dart';

part 'transaksi.g.dart';

@JsonSerializable()
class Transaksi {
  Transaksi();

  int id;
  String jenisTransaksi;
  String keterangan;
  String tanggal;
  int kategori;
  num pemasukan;
  num pengeluaran;

  factory Transaksi.fromJson(Map<String, dynamic> json) =>
      _$TransaksiFromJson(json);
  Map<String, dynamic> toJson() => _$TransaksiToJson(this);
}
