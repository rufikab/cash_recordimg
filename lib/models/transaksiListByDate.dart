import 'package:json_annotation/json_annotation.dart';
import "transaksiByDate.dart";
part 'transaksiListByDate.g.dart';

@JsonSerializable(nullable: false)
class TransaksiListByDate {
  TransaksiListByDate();

  List<TransaksiByDate> transaksiList;

  factory TransaksiListByDate.fromJson(Map<String, dynamic> json) =>
      _$TransaksiListByDateFromJson(json);
  Map<String, dynamic> toJson() => _$TransaksiListByDateToJson(this);
}
