// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'kategoriList.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

KategoriList _$KategoriListFromJson(Map<String, dynamic> json) {
  return KategoriList()
    ..kategoriList = (json['kategoriList'] as List)
        ?.map((e) =>
            e == null ? null : Kategori.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$KategoriListToJson(KategoriList instance) =>
    <String, dynamic>{'kategoriList': instance.kategoriList};
