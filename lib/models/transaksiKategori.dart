import 'package:json_annotation/json_annotation.dart';
part 'transaksiKategori.g.dart';

@JsonSerializable(nullable: false)
class TransaksiKategori {
  TransaksiKategori();
  int id;
  String jenisTransaksi;
  String keterangan;
  String tanggal;
  int kategori;
  num pemasukan;
  num pengeluaran;
  String kategoriNama;

  factory TransaksiKategori.fromJson(Map<String, dynamic> json) =>
      _$TransaksiKategoriFromJson(json);
  Map<String, dynamic> toJson() => _$TransaksiKategoriToJson(this);
}
