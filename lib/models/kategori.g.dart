// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'kategori.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Kategori _$KategoriFromJson(Map<String, dynamic> json) {
  return Kategori()
    ..id = json['id'] as int
    ..keterangan = json['keterangan'] as String
    ..jenis = json['jenis'] as int;
}

Map<String, dynamic> _$KategoriToJson(Kategori instance) => <String, dynamic>{
      'id': instance.id,
      'keterangan': instance.keterangan,
      'jenis': instance.jenis
    };
