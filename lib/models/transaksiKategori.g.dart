// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaksiKategori.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************
TransaksiKategori _$TransaksiKategoriFromJson(Map<String, dynamic> json) {
  return TransaksiKategori()
    ..id = json['id'] as int
    ..jenisTransaksi = json['jenisTransaksi'] as String
    ..keterangan = json['keterangan'] as String
    ..tanggal = json['tanggal'] as String
    ..kategori = json['kategori'] as int
    ..pemasukan = json['pemasukan'] as num
    ..pengeluaran = json['pengeluaran'] as num
    ..kategoriNama = json['kategoriNama'] as String;
}

Map<String, dynamic> _$TransaksiKategoriToJson(TransaksiKategori instance) =>
    <String, dynamic>{
      'id': instance.id,
      'jenisTransaksi': instance.jenisTransaksi,
      'keterangan': instance.keterangan,
      'tanggal': instance.tanggal,
      'kategori': instance.kategori,
      'pemasukan': instance.pemasukan,
      'pengeluaran': instance.pengeluaran,
      'kategori_nama': instance.kategoriNama,
    };
