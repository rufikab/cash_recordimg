import 'package:cash_recording/screen/rangkuman/rangkuman_view.dart';
import 'package:flutter/material.dart';
import 'package:cash_recording/constant.dart';
import 'package:cash_recording/screen/beranda/beranda_view.dart';
import 'package:cash_recording/screen/transaksi/transaksi_view.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:cash_recording/models/index.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => new _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  TextEditingController controller = TextEditingController();
  String name;
  int curId;
  TransaksiKategori t = new TransaksiKategori();
  final formKey = new GlobalKey<FormState>();
  int _bottomNavCurrentIndex = 0;
  List<Widget> _container = [
    new BerandaPage(),
    new RangkumanPage(
      initialDate: DateTime.now(),
    ),
  ];

  @override
  void initState() {
    super.initState();
    _bottomNavCurrentIndex = 0;
  }

  refreshList() {
    setState(() {
      // transaksiList = dbHelper.getTransaksiList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: _container[_bottomNavCurrentIndex],
      floatingActionButton: FloatingActionButton(
        heroTag: "btnAdd",
        child: Icon(Icons.add, color: Colors.white),
        backgroundColor: ColorPalette.colorBtnActive,
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => TransaksiPage(false, t)),
          ).then(
            (value) => setState(() {
              Navigator.pushReplacement(
                  context,
                  new MaterialPageRoute(
                      builder: (BuildContext context) => new LandingPage()));
            }),
          );
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: _floatingNavigationBar(),
    );
  }

  Widget _floatingNavigationBar() {
    return new BottomAppBar(
        notchMargin: 10,
        shape: CircularNotchedRectangle(),
        color: ColorPalette.bgColorPrimary,
        child: BottomNavigationBar(
          onTap: (index) {
            print(index);
            setState(() {
              _bottomNavCurrentIndex = index;
            });
          },
          elevation: 0,
          backgroundColor: Colors.transparent,
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                color: ColorPalette.grey1,
              ),
              title: new Text(
                "Beranda",
                style: TextStyle(color: ColorPalette.grey1),
              ),
              backgroundColor: Colors.white,
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.book,
                color: ColorPalette.grey1,
              ),
              title: Text(
                "Rangkuman",
                style: TextStyle(color: ColorPalette.grey1),
              ),
            )
          ],
        ));
  }

  Widget _curvedNavigationBar() {
    return CurvedNavigationBar(
      backgroundColor: Colors.transparent,
      color: ColorPalette.colorPrimary,
      buttonBackgroundColor: ColorPalette.colorPrimary,
      height: 60,
      animationDuration: Duration(milliseconds: 200),
      index: 2,
      animationCurve: Curves.bounceInOut,
      items: <Widget>[
        Icon(Icons.favorite, size: 30, color: Colors.white),
        Icon(Icons.verified_user, size: 30, color: Colors.white),
        Icon(Icons.home, size: 30, color: Colors.white),
      ],
      onTap: (index) {},
    );
  }
}
