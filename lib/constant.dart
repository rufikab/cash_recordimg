import 'package:flutter/material.dart';

class ColorPalette {
  static Color bgColorPrimary = Colors.white;
  static Color bgGrey = Color.fromARGB(255, 201, 201, 201);
  static Color grey1 = Color.fromARGB(255, 124, 116, 116);
  // static Color colorPrimary = Color.fromARGB(255, 24, 48, 230);
  static Color colorBtnActive = Color.fromARGB(255, 249, 208, 109);
  static Color colorPrimary = Color.fromARGB(255, 249, 208, 109);
  static Color colorBox1 = Color.fromARGB(255, 245, 178, 17);
  static Color colorBox2 = Color.fromARGB(255, 130, 140, 234);
  //static Color colorBox2 = Color.fromARGB(255, 24, 48, 230);
  static Color grey200 = Color.fromARGB(200, 242, 242, 242);
  static Color green = Color.fromARGB(255, 115, 198, 185);
  static Color pink = Color.fromARGB(255, 254, 137, 139);
  static Color grey = Color.fromARGB(255, 242, 242, 242);
  static Color red = Color(0xffec1d27);
}
